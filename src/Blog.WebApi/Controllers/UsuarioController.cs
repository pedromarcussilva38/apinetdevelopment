﻿using Blog.WebApi.Interface.InterfaceService;
using Blog.WebApi.Models;
using Blog.WebApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Blog.WebApi.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioService _service;
        public UsuarioController(IUsuarioService service)
        {
            _service = service;
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Autenticacao([FromBody] Usuario dadosAcesso) 
        {
            var usuario = await _service.GetUsuarioParaAutenticacao(dadosAcesso.Username, dadosAcesso.Senha);

            if (usuario == null)
                return NotFound(new { message = "Usuário ou senha inválidos" });

            var token = TokenService.GenerateToken(usuario);
            usuario.Senha = "";

            return new 
            {
                usuario = usuario, 
                token = token
            }; 
        }

        [HttpGet]
        [Authorize]
        public async Task<ICollection<Usuario>> GetUsuariosAsync()
        {
            var usuarios = await _service.GetUsuarios();
            return usuarios;
        }

        [HttpPost]
        [Route("salvar")]
        [AllowAnonymous]
        public async Task<IActionResult> SalvarUsuarioAsync([FromBody] Usuario usuario)
        {
            if (!await _service.ExisteUsuarioComMesmoUsername(usuario.Username))
            {
                await _service.SalvarUsuario(usuario);

                return Ok();
            }
            else
                return BadRequest("Já existe um usuário com este username.");         
        }
    }
}
