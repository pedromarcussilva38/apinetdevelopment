﻿using Blog.WebApi.Interface.InterfaceService;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Blog.WebApi.Models;

namespace Blog.WebApi.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class PostController : Controller
    {
        private readonly IPostService _service;
        public PostController(IPostService service)
        {
            _service = service;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("posts")]
        public async Task<ICollection<Post>> GetPosts()
        {
            return await _service.GetPosts();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("post")]
        public async Task<dynamic> GetPosts(int id)
        {
            var post = await _service.GetPost(id);

            if (post == null)
                return BadRequest("Nenhum post encontrado.");

            return post;
        }

        [HttpPost]
        [Authorize]
        [Route("adicionar")]
        public async Task<dynamic> CriarPostAsync([FromBody] Post post)
        {
            try
            {
                await _service.CriarPostAsync(post, User.Identity.Name);        
                return Ok();
            }
            catch
            {
                return BadRequest("Não foi possível criar um novo Post");
            }
        }
    }
}
