﻿using Blog.WebApi.Interface.InterfaceService;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Blog.WebApi.Models;

namespace Blog.WebApi.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class AlbumController : Controller
    {
        private readonly IAlbumService _service;
        public AlbumController(IAlbumService service)
        {
            _service = service;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ICollection<Album>> Album()
        {
            var albuns = await _service.GetAlbuns();
            return albuns;
        }

        [HttpPost]
        [Authorize]
        public async Task<dynamic> CriarAlbumAsync([FromBody] Album album) 
        {
            try
            {
                await _service.CriarAlbumAsync(album, User.Identity.Name);
                return Ok();
            }
            catch
            {
                return BadRequest("Não foi possível cadastrar o Album: " + album.Nome); 
            }
        }
    }
}
