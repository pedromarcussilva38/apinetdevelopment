﻿using Blog.WebApi.Interface.InterfaceService;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Blog.WebApi.Models;

namespace Blog.WebApi.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class FotoController : Controller
    {
        private readonly IFotoService _service;

        public FotoController(IFotoService service)
        {
            _service = service;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ICollection<Foto>> GetFotos()
        {
            return await _service.GetFotos();
        }

        [HttpPost]
        [Authorize]
        [Route("adicionar")]
        public async Task<dynamic> CriarFotoAsync([FromBody] Foto foto)
        {
            try
            {
                if (await _service.AlbumPertenceUsuarioLogado(foto.IdAlbum, User.Identity.Name))
                    await _service.AdicionarFotoAsync(foto);
                else
                    return BadRequest("O usuário logado não é propietário do album informado.");

                return Ok();
            }
            catch
            {
                return BadRequest("Não foi possível cadastrar a foto: " + foto.Nome);
            }
        }
    }
}
