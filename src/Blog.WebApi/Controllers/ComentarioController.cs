﻿using Blog.WebApi.Interface.InterfaceService;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Blog.WebApi.Models;

namespace Blog.WebApi.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class ComentarioController : Controller
    {

        private readonly IComentarioService _service; 
        public ComentarioController(IComentarioService service)
        {
            _service = service;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("post")]
        public async Task<dynamic> GetComentarioPorPost(int id)
        {
            var comentario = await _service.GetComentariosPorPost(id);

            if (comentario == null)
                return BadRequest("Nenhum comentário encontrado.");

            return comentario;
        }

        [HttpPost]
        [Authorize]
        [Route("adicionar")]
        public async Task<dynamic> CriarPostAsync([FromBody] Comentario comentario)
        {
            try
            {
                await _service.CriarComentarioAsync(comentario, User.Identity.Name);
                return Ok();
            }
            catch
            {
                return BadRequest("Não foi possível criar um novo Comentário");
            }
        }
    }
}
