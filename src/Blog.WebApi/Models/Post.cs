﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Models
{
    [Table("post")]
    public class Post
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("idusuario")]
        public int IdUsuario { get; set; }

        [Column("titulo")]
        public string Titulo { get; set; }

        [Column("descricao")]
        public string Descricao { get; set; } 
    }
}
