﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Models
{
    [Table("foto")]
    public class Foto
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("nome")]
        public string Nome { get; set; }

        [Column("conteudo")]
        public string Conteudo { get; set; }

        [NotMapped]
        public int IdAlbum { get; set; }
    }
}
