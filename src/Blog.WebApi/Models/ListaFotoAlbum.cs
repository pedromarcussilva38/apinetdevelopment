﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Models
{
    [Table("listafotoalbum")]
    public class ListaFotoAlbum
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("idalbum")]
        public int IdAlbum { get; set; }

        [Column("idfoto")]
        public int IdFoto { get; set; }
    }
}
