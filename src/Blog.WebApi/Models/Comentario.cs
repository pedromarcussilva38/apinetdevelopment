﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Models
{
    [Table("comentario")]
    public class Comentario
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("descricao")]
        public string Descricao { get; set; }

        [Column("idusuario")]
        public int IdUsuario { get; set; }

        [Column("idpost")]
        public int IdPost { get; set; }
    }
}
