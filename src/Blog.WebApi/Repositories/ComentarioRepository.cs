﻿using Blog.WebApi.Interface.InterfaceRepository;
using Blog.WebApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Repositories
{
    public class ComentarioRepository : IComentarioRepository
    {
        private readonly Contexto _contexto;

        public ComentarioRepository(Contexto context)
        {
            _contexto = context;
        }

        public async Task<List<Comentario>> GetComentariosPorPost(int idPost) 
        {
            return await _contexto.Comentarios.Where(x => x.IdPost == idPost).ToListAsync();
        } 

        public async Task CriarComentarioAsync(Comentario comentario) 
        {
            await _contexto.Comentarios.AddAsync(comentario);
            await _contexto.SaveChangesAsync();
        }
    }
}
