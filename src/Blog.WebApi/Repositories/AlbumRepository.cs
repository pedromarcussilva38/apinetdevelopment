﻿using Blog.WebApi.Interface.InterfaceRepository;
using Blog.WebApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Repositories
{
    public class AlbumRepository : IAlbumRepository
    {
        private readonly Contexto _contexto;

        public AlbumRepository(Contexto context)
        {
            _contexto = context;
        }

        public async Task<List<Album>> GetAlbuns() 
        {
            return await _contexto.Albums.ToListAsync();
        }
        public async Task<Album> GetAlbumPorId(int Id)
        {
            return await _contexto.Albums.Where(x => x.Id == Id).FirstOrDefaultAsync();
        }

        public async Task CriarAlbumAsync(Album album) 
        {
            await _contexto.AddAsync(album);
            await _contexto.SaveChangesAsync();
        }
    }
}
