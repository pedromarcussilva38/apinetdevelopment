﻿using Blog.WebApi.Interface.InterfaceRepository;
using Blog.WebApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Repositories
{
    public class FotoRepository : IFotoRepository
    {
        private readonly Contexto _contexto;
        private readonly IListaFotoAlbumRepository _listaFotoAlbumRepositorio;

        public FotoRepository(Contexto context, IListaFotoAlbumRepository listaFotoAlbumRepositorio)
        {
            _contexto = context;
            _listaFotoAlbumRepositorio = listaFotoAlbumRepositorio;
        }

        public async Task<List<Foto>> GetFotos() 
        {
            return await _contexto.Fotos.ToListAsync();
        }

        public async Task AdicionarFotoAsync(Foto foto)
        {
            await _contexto.Fotos.AddAsync(foto);
            await _contexto.SaveChangesAsync();
            await _listaFotoAlbumRepositorio.AdicionarFotoEmListaDeAlbum(foto.Id, foto.IdAlbum);
        }
    }
}
