﻿using Blog.WebApi.Interface.InterfaceRepository;
using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Repositories
{
    public class ListaFotoAlbumRepository : IListaFotoAlbumRepository
    {
        private readonly Contexto _contexto;

        public ListaFotoAlbumRepository(Contexto context)
        {
            _contexto = context;
        }

        public async Task AdicionarFotoEmListaDeAlbum(int idFoto, int IdAlbum) 
        {
            var fotoEmAlbum = new ListaFotoAlbum();
            fotoEmAlbum.IdFoto = idFoto;
            fotoEmAlbum.IdAlbum = IdAlbum;

            await _contexto.ListaFotoAlbums.AddAsync(fotoEmAlbum);
            await _contexto.SaveChangesAsync();
        }
    }
}
