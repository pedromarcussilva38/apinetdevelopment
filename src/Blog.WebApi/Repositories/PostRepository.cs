﻿using Blog.WebApi.Interface.InterfaceRepository;
using Blog.WebApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Repositories
{
    public class PostRepository : IPostRepository
    {

        private readonly Contexto _contexto;

        public PostRepository(Contexto context)
        {
            _contexto = context;
        }

        public async Task<Post> GetPost(int id)
        {
            return await _contexto.Posts.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<List<Post>> GetPosts() 
        {
            return await _contexto.Posts.ToListAsync();
        }

        public async Task CriarPostAsync(Post post) 
        {
            await _contexto.Posts.AddAsync(post);
            await _contexto.SaveChangesAsync();
        }
    }
}
