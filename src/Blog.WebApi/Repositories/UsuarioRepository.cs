﻿using Blog.WebApi.Interface.InterfaceRepository;
using Blog.WebApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly Contexto _contexto; 

        public UsuarioRepository(Contexto context)
        {
            _contexto = context;
        }

        public async Task<List<Usuario>> GetUsuarios()
        {
            var usuarios = await _contexto.Usuarios.ToListAsync();
            usuarios.ForEach(x => x.Senha = string.Empty);

            return usuarios;
        }

        public async Task SalvarUsuario(Usuario usuario) 
        {
            await _contexto.Usuarios.AddAsync(usuario);
            await _contexto.SaveChangesAsync();
        }

        public async Task<bool> ExisteUsuarioComMesmoUsername(string username) 
        {
            var usuario = await _contexto.Usuarios.Where(x => x.Username == username).FirstOrDefaultAsync();
            if (usuario != null)
                return true;

            return false;
        }

        public async Task<int> GetIdUsuarioPorUsername(string username)
        {
            return await _contexto.Usuarios.Where(x => x.Username == username).Select(x => x.Id).FirstOrDefaultAsync();
        }

        public async Task<Usuario> GetUsuarioParaAutenticacao(string username, string senha) 
        {
            var usuario = await _contexto.Usuarios.Where(x => x.Username == username && x.Senha == senha).FirstOrDefaultAsync();
            return usuario;
        }
    }
}
