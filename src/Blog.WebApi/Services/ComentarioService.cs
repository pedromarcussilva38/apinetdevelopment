﻿using Blog.WebApi.Interface.InterfaceRepository;
using Blog.WebApi.Interface.InterfaceService;
using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Services
{
    public class ComentarioService : IComentarioService
    {
        private readonly IComentarioRepository _repositorioComentario;
        private readonly IUsuarioRepository _repositorioUsuario;
        public ComentarioService(IComentarioRepository repositorioComentario, IUsuarioRepository repositorioUsuario)
        {
            _repositorioComentario = repositorioComentario;
            _repositorioUsuario = repositorioUsuario;
        }

        public async Task CriarComentarioAsync(Comentario comentario, string usernameUsuarioLogado)
        {
            var idUsuarioLogado = await _repositorioUsuario.GetIdUsuarioPorUsername(usernameUsuarioLogado);
            comentario.IdUsuario = idUsuarioLogado;

            await _repositorioComentario.CriarComentarioAsync(comentario);
        }

        public async Task<List<Comentario>> GetComentariosPorPost(int idPost)
        {
            return await _repositorioComentario.GetComentariosPorPost(idPost);
        }
    }
}
