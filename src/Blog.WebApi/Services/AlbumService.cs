﻿using Blog.WebApi.Interface.InterfaceRepository;
using Blog.WebApi.Interface.InterfaceService;
using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Services
{
    public class AlbumService : IAlbumService
    {
        private readonly IAlbumRepository _repositorioAlbum;
        private readonly IUsuarioRepository _repositorioUsuario;
        public AlbumService(IAlbumRepository repositorio, IUsuarioRepository repositorioUsuario)
        {
            _repositorioAlbum = repositorio;
            _repositorioUsuario = repositorioUsuario; 
        }

        public async Task CriarAlbumAsync(Album album, string username)
        {
            album.IdUsuario = await _repositorioUsuario.GetIdUsuarioPorUsername(username);
            await _repositorioAlbum.CriarAlbumAsync(album);
        }

        public async Task<List<Album>> GetAlbuns()
        {
            return await _repositorioAlbum.GetAlbuns();
        }
    }
}
