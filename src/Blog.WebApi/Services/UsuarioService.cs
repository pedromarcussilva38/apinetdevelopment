﻿using Blog.WebApi.Interface.InterfaceRepository;
using Blog.WebApi.Interface.InterfaceService;
using Blog.WebApi.Models;
using Blog.WebApi.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepositorio;

        public UsuarioService(IUsuarioRepository repositorio)
        {
            _usuarioRepositorio = repositorio;
        }

        public async Task<List<Usuario>> GetUsuarios() 
        {
            return await _usuarioRepositorio.GetUsuarios();
        }

        public async Task SalvarUsuario(Usuario usuario) 
        {
            await _usuarioRepositorio.SalvarUsuario(usuario);
        }

        public async Task<bool> ExisteUsuarioComMesmoUsername(string username) 
        {
            return await _usuarioRepositorio.ExisteUsuarioComMesmoUsername(username);
        }

        public async Task<Usuario> GetUsuarioParaAutenticacao(string username, string senha) 
        {
            return await _usuarioRepositorio.GetUsuarioParaAutenticacao(username, senha);
        }
    }
}
