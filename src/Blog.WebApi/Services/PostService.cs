﻿using Blog.WebApi.Interface.InterfaceRepository;
using Blog.WebApi.Interface.InterfaceService;
using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Services
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _repositorioPost;
        private readonly IUsuarioRepository _repositorioUsuario;
        public PostService(IPostRepository repositorioPost, IUsuarioRepository repositorioUsuario)
        {
            _repositorioPost = repositorioPost;
            _repositorioUsuario = repositorioUsuario;
        }

        public async Task CriarPostAsync(Post post, string usernameUsuarioLogado)
        {
            var idUsuarioLogado = await _repositorioUsuario.GetIdUsuarioPorUsername(usernameUsuarioLogado);
            post.IdUsuario = idUsuarioLogado;

            await _repositorioPost.CriarPostAsync(post);
        }

        public async Task<Post> GetPost(int id)
        {
            return await _repositorioPost.GetPost(id);
        }

        public async Task<List<Post>> GetPosts()
        {
            return await _repositorioPost.GetPosts();
        }
    }
}
