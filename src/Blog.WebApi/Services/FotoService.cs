﻿using Blog.WebApi.Interface.InterfaceRepository;
using Blog.WebApi.Interface.InterfaceService;
using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Services
{
    public class FotoService : IFotoService
    {
        private readonly IFotoRepository _repositorioFoto;
        private readonly IAlbumRepository _repositorioAlbum;
        private readonly IUsuarioRepository _repositorioUsuario;
        public FotoService(IFotoRepository repositorioFoto, IAlbumRepository repositorioAlbum, IUsuarioRepository repositorioUsuario)
        {
            _repositorioFoto = repositorioFoto;
            _repositorioAlbum = repositorioAlbum;
            _repositorioUsuario = repositorioUsuario;
        }

        public async Task<List<Foto>> GetFotos() 
        {
            return await _repositorioFoto.GetFotos();
        }

        public async Task AdicionarFotoAsync(Foto foto)
        {
            await _repositorioFoto.AdicionarFotoAsync(foto);
        }

        public async Task<bool> AlbumPertenceUsuarioLogado(int idAlbum, string usernameUsuarioLogado)
        {
            var idUsuario = await _repositorioUsuario.GetIdUsuarioPorUsername(usernameUsuarioLogado);
            var album = await _repositorioAlbum.GetAlbumPorId(idAlbum);

            if (album != null)
            {
                if (album.IdUsuario == idUsuario)
                    return true;
            }

            return false;
        }
    }
}
