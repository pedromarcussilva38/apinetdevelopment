﻿using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Interface.InterfaceService
{
    public interface IFotoService
    {
        Task<List<Foto>> GetFotos();
        Task AdicionarFotoAsync(Foto foto);
        Task<bool> AlbumPertenceUsuarioLogado(int idAlbum, string usernameUsuarioLogado);
    }
}
