﻿using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Interface.InterfaceService
{
    public interface IPostService
    {
        Task<Post> GetPost(int id);
        Task<List<Post>> GetPosts();
        Task CriarPostAsync(Post post, string usernameUsuarioLogado);
    }
}
