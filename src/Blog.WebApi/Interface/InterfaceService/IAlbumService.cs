﻿using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Interface.InterfaceService
{
    public interface IAlbumService
    {
        Task<List<Album>> GetAlbuns();
        Task CriarAlbumAsync(Album album, string username);
    }
}
