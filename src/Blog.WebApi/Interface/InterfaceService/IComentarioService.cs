﻿using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Interface.InterfaceService
{
    public interface IComentarioService
    {
        Task<List<Comentario>> GetComentariosPorPost(int idPost);
        Task CriarComentarioAsync(Comentario comentario, string usernameUsuarioLogado);
    }
}
