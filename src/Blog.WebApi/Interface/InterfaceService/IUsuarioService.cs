﻿using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Interface.InterfaceService
{
    public interface IUsuarioService
    {
        Task<List<Usuario>> GetUsuarios();
        Task SalvarUsuario(Usuario usuario);
        Task<bool> ExisteUsuarioComMesmoUsername(string username);
        Task<Usuario> GetUsuarioParaAutenticacao(string username, string senha);
    }
}
