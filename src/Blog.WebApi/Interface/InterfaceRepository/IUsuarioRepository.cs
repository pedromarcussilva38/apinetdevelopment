﻿using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Interface.InterfaceRepository
{
    public interface IUsuarioRepository
    {
        Task<List<Usuario>> GetUsuarios();
        Task SalvarUsuario(Usuario usuario);
        Task<bool> ExisteUsuarioComMesmoUsername(string username);
        Task<Usuario> GetUsuarioParaAutenticacao(string username, string senha);
        Task<int> GetIdUsuarioPorUsername(string username); 
    }
}
