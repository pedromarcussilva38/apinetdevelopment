﻿using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Interface.InterfaceRepository
{
    public interface IFotoRepository
    {
        Task<List<Foto>> GetFotos();
        Task AdicionarFotoAsync(Foto foto);
    }
}
