﻿using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Interface.InterfaceRepository
{
    public interface IPostRepository
    {
        Task<Post> GetPost(int id);
        Task<List<Post>> GetPosts();
        Task CriarPostAsync(Post post);
    }
}
