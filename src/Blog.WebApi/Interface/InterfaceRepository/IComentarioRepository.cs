﻿using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Interface.InterfaceRepository
{
    public interface IComentarioRepository
    {
        Task<List<Comentario>> GetComentariosPorPost(int idPost);
        Task CriarComentarioAsync(Comentario comentario);
    }
}
