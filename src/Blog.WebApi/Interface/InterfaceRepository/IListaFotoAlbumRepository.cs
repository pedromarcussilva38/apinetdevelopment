﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Interface.InterfaceRepository
{
    public interface IListaFotoAlbumRepository
    {
        Task AdicionarFotoEmListaDeAlbum(int idFoto, int IdAlbum);
    }
}
