﻿using Blog.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.WebApi.Interface.InterfaceRepository
{
    public interface IAlbumRepository
    {
        Task<List<Album>> GetAlbuns();
        Task CriarAlbumAsync(Album album);
        Task<Album> GetAlbumPorId(int Id);
    }
}
