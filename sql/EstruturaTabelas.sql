Create Database db_blog; 

Create Table Usuario(
Id serial primary key, 
Nome varchar(200), 
Username varchar(100), 
Senha varchar(100)
); 

Create Table Post(
Id serial primary key, 
Titulo varchar(30), 
Descricao varchar(1000),
IdUsuario int, 
Foreign Key (IdUsuario) References Usuario(id)
); 

Create Table Comentario(
Id serial primary key,
IdUsuario int, 
IdPost int, 
Descricao varchar(200), 
Foreign Key (IdUsuario) References Usuario (Id),
Foreign Key (IdPost) References Post (Id) 
);

Create Table Album(
Id serial primary key, 
Nome varchar(100),
IdUsuario int, 
Foreign Key (IdUsuario) References Usuario (Id)
);

Create Table Foto(
Id serial primary key, 
Nome varchar(100),
Conteudo varchar(1000)
); 

Create Table ListaFotoAlbum(
Id serial primary key, 
IdAlbum int, 
IdFoto int, 
Foreign Key (IdAlbum) References Album(Id),
Foreign Key (IdFoto) References Foto (Id) 
);


Listar tabelas \d

